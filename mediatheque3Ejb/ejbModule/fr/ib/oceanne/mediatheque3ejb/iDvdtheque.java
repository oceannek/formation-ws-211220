package fr.ib.oceanne.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

//description des fonctionnalité d'EJB à distance
@Remote
public interface iDvdtheque {
	public String getInfos();
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation();

}
