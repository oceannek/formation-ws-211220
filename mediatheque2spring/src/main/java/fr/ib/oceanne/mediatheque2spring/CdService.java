package fr.ib.oceanne.mediatheque2spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component //ou @service
public class CdService {
	private List<Cd> cds;
	
	public CdService() {
		cds = new ArrayList<Cd>();
	}
	
	public int getNombreDeCd () {
		return cds.size();
	}
	
	public void ajouteCd(Cd cd) {
		cds.add(cd);
	}

	public List<Cd> getCds() {
		return cds;
	}
	//n :numero de cd
	public Cd getCd(int n) {
		return cds.get(n);
	}
	
	public void changerAnnee(int n, int annee) {
		cds.get(n).setAnnee(annee);
	}
	
	
	
	public void changerTitre(int n, String titre) {
		 cds.get(n).setTitre(titre);
	}
	
}
