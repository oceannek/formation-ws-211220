package fr.ib.oceanne.mediatheque2spring.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			//lecture du contenue 
			System.out.println("Client JAVA");
			URL url = new URL ("http://localhost:9002/health");
			//connextion vers le serveur web
			URLConnection conn =url.openConnection();
			//Création de flux en entrée permettant de lire ce que me renvoie le serveur en binaire
			InputStream is= conn.getInputStream();
			//conversion en flux texte lisible ligne par ligne
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String reponse = br.readLine();
			System.out.println("Health :" +reponse);
			br.close();
			
		
		 
		} catch (Exception ex){
			System.out.println("Erreur " +ex);
		}

	}

}
