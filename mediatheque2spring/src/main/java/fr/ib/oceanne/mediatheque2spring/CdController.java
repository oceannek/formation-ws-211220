package fr.ib.oceanne.mediatheque2spring;

import java.util.List;

import javax.swing.text.html.FormSubmitEvent.MethodType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.ib.oceanne.mediatheque2spring.CdService;
import fr.ib.oceanne.mediatheque2spring.CdController;

@RestController
public class CdController {
	private static final Logger logger = LoggerFactory.getLogger(CdController.class);
	private CdService cdService;
	
	@Autowired
	public void setCdService(CdService cdService) {
	this.cdService = cdService;
	}
	
	//Ajout de path quand plusieurs paramêtres, format de réponse en xml
	@RequestMapping(path="/cd/nombre",produces =MediaType.APPLICATION_XML_VALUE, method=RequestMethod.GET)
	public int getNombreDeCd() {
		logger.warn("affichage du nombre de Cd");
		return cdService.getNombreDeCd();
	}
	
	@RequestMapping(path="/cd",method=RequestMethod.POST)
		//requestbody : le CD est à l'intérieur du corps de la requete fabriqué par le client
		public void ajoute (@RequestBody Cd cd) {
			cdService.ajouteCd(cd);
	}
	
	@RequestMapping(path="/cd", method=RequestMethod.GET)
		public List<Cd> getTous(){
			return cdService.getCds();
	}
	
	// @pathVariable argument num va remplir le n
	// num est composé de 1 ou plusieurs chiffres : \\d+
	//num est composé de lettres et de chiffres : \\W
	@RequestMapping(path="/cd/{num:\\d+}", method=RequestMethod.GET)
	public Cd getUn(@PathVariable("num") int n) {
		return cdService.getCd(n);
	}
	
	@RequestMapping(path="/cd/{num:\\d+}/titre", method=RequestMethod.PUT)
	public void changerTitre(@PathVariable("num") int n, @RequestBody String titre)  {
		cdService.changerTitre(n, titre);
	}
	
	@RequestMapping(path="/cd/{num:\\d+}/annee", method=RequestMethod.PUT)
	public void changerAnnee(@PathVariable("num") int n,@RequestBody int annee)  {
		cdService.changerAnnee(n, annee);
	}
	
	@RequestMapping(path="/json/cd", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> getTousJson(){
		return cdService.getCds();
	}
}
