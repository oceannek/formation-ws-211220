package fr.ib.oceanne.mediatheque2spring.clients;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.oceanne.mediatheque2spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			//lecture du contenue 
			System.out.println("Client java Spring");
			RestTemplate rt = new RestTemplate();
			//je rend la réponse sous forme de chaine de charactère
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health :" +health);
			Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Il y a "+nbCd+" cd à la médiatheque");
			Cd cd1 = new Cd ("Abbey Road", "The Beatles", 1966);
			Cd cd2 = new Cd ("LA MAGIE DISNEY", "BANDE ORIGINALE DE FILM", 2020);
			Cd cd3 = new Cd ("LES PLUS BELLES COMPTINES D'OKOO", "COMPILATION", 2000);
			Cd cd4 = new Cd ("Les fables de la fontaine en chansons", "WE ARE WORLD CITIZENS", 2021);
			//On envoie une donnée à Cd. La fonction void ne renvoie rien: il n y a pas de resultat
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);
			rt.postForObject("http://localhost:9002/cd", cd2, Void.class);
			rt.postForObject("http://localhost:9002/cd", cd3, Void.class);
			rt.postForObject("http://localhost:9002/cd", cd4, Void.class);
			
			//Ne fonctionne pas
			// List<Cd> cds= rt.getForObject("http://localhost:9002/cd/nombre", List<Cd>.class);
			
			// Solution 1 :
			// objet qui permet de récupérer le type de la réponse (Block de liste de cd)
			// ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {
			// };
			// ResponseEntity<List<Cd>> cdsEntity =rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
			// System.out.println("Tous les CD :");
			// List<Cd> cds = cdsEntity.getBody();
			
			//Solution 2:
			//Les tableaux remplace les listes et donc fonctionnent
			Cd[] cds= rt.getForObject("http://localhost:9002/cd", Cd[].class);
			System.out.println("Tous les CD :");
			for (Cd cd : cds) {
				System.out.println( "-" +cd);
			}
			
			//paramète de requète : "http://localhost:9002/cd?no=0"
			Cd cd0= rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier Cd: " +cd0);
		
			
			//change le titre du premier cd
			rt.put("http://localhost:9002/cd/0/titre", "help!", String.class);
			
			//changer l'annee
			rt.put("http://localhost:9002/cd/2/annee", 2033, int.class);
			
		
			
		} catch (Exception ex){
			System.out.println("Erreur " +ex);
		}

	}

}
