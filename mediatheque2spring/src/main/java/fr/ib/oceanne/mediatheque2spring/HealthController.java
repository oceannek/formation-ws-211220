package fr.ib.oceanne.mediatheque2spring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthController {
	
	@RequestMapping("/health")
	//utile pour que le client vérifie si le controller fonctionne bien
	public String getHealth() {
		return "OK";
	}

}
