package fr.ib.oceanne.mediatheque1cxf.serveur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

//code des processus du service

@WebService
public class LivresService implements ILivresService{
	public String getInfos() {
		return "L'entrée de la bibliotheque se trouve avenue Victor Hugo";
	}

	public boolean estEmpruntable(int id) {
		if (id<1)
			throw new IllegalArgumentException("id doit être 1 ou plus");
		return false;
	}

	public Date getRetour(int id) {
		//renvoyer toujours : aujourd'hui plus 10j
		//LocalDate d = LocalDate.now();
		//return d.plusDays(10);
		//la classe localDate n'est pas un java bean donc cxf génère une exception, il faut ainsi transformer la date.
		return Date.from(LocalDate.now().plusDays(10).atStartOfDay(ZoneId.systemDefault()).toInstant());
	
	}

	public Livre getLivreDuMois() {
		Livre l = new Livre ("Dieu - la science - les preuves ", "Michel-Yves Bolloré", 2021);
		DataSource dataSource = new FileDataSource("dieu-la-science-les-preuves.jpg");
		DataHandler dataHandler = new DataHandler(dataSource);
		l.setImage(dataHandler);
		
		return l;
	}

	public Livre[] getLivreDeLannee() {
		Livre[] livres = new Livre[12];
		for (int i=0; i<12; i++) {
			livres [i] = getLivreDuMois();
		}
		return livres;
	}

}
