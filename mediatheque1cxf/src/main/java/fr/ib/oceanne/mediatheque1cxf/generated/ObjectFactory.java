
package fr.ib.oceanne.mediatheque1cxf.generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.ib.oceanne.mediatheque1cxf.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EstEmpruntable_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "estEmpruntable");
    private final static QName _EstEmpruntableResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "estEmpruntableResponse");
    private final static QName _GetInfos_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getInfos");
    private final static QName _GetInfosResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getInfosResponse");
    private final static QName _GetLivreDeLannee_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getLivreDeLannee");
    private final static QName _GetLivreDeLanneeResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getLivreDeLanneeResponse");
    private final static QName _GetLivreDuMois_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getLivreDuMois");
    private final static QName _GetLivreDuMoisResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getLivreDuMoisResponse");
    private final static QName _GetRetour_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getRetour");
    private final static QName _GetRetourResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceanne.ib.fr/", "getRetourResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.ib.oceanne.mediatheque1cxf.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EstEmpruntable }
     * 
     */
    public EstEmpruntable createEstEmpruntable() {
        return new EstEmpruntable();
    }

    /**
     * Create an instance of {@link EstEmpruntableResponse }
     * 
     */
    public EstEmpruntableResponse createEstEmpruntableResponse() {
        return new EstEmpruntableResponse();
    }

    /**
     * Create an instance of {@link GetInfos }
     * 
     */
    public GetInfos createGetInfos() {
        return new GetInfos();
    }

    /**
     * Create an instance of {@link GetInfosResponse }
     * 
     */
    public GetInfosResponse createGetInfosResponse() {
        return new GetInfosResponse();
    }

    /**
     * Create an instance of {@link GetLivreDeLannee }
     * 
     */
    public GetLivreDeLannee createGetLivreDeLannee() {
        return new GetLivreDeLannee();
    }

    /**
     * Create an instance of {@link GetLivreDeLanneeResponse }
     * 
     */
    public GetLivreDeLanneeResponse createGetLivreDeLanneeResponse() {
        return new GetLivreDeLanneeResponse();
    }

    /**
     * Create an instance of {@link GetLivreDuMois }
     * 
     */
    public GetLivreDuMois createGetLivreDuMois() {
        return new GetLivreDuMois();
    }

    /**
     * Create an instance of {@link GetLivreDuMoisResponse }
     * 
     */
    public GetLivreDuMoisResponse createGetLivreDuMoisResponse() {
        return new GetLivreDuMoisResponse();
    }

    /**
     * Create an instance of {@link GetRetour }
     * 
     */
    public GetRetour createGetRetour() {
        return new GetRetour();
    }

    /**
     * Create an instance of {@link GetRetourResponse }
     * 
     */
    public GetRetourResponse createGetRetourResponse() {
        return new GetRetourResponse();
    }

    /**
     * Create an instance of {@link Livre }
     * 
     */
    public Livre createLivre() {
        return new Livre();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstEmpruntable }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EstEmpruntable }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "estEmpruntable")
    public JAXBElement<EstEmpruntable> createEstEmpruntable(EstEmpruntable value) {
        return new JAXBElement<EstEmpruntable>(_EstEmpruntable_QNAME, EstEmpruntable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstEmpruntableResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EstEmpruntableResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "estEmpruntableResponse")
    public JAXBElement<EstEmpruntableResponse> createEstEmpruntableResponse(EstEmpruntableResponse value) {
        return new JAXBElement<EstEmpruntableResponse>(_EstEmpruntableResponse_QNAME, EstEmpruntableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInfos }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetInfos }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getInfos")
    public JAXBElement<GetInfos> createGetInfos(GetInfos value) {
        return new JAXBElement<GetInfos>(_GetInfos_QNAME, GetInfos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInfosResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetInfosResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getInfosResponse")
    public JAXBElement<GetInfosResponse> createGetInfosResponse(GetInfosResponse value) {
        return new JAXBElement<GetInfosResponse>(_GetInfosResponse_QNAME, GetInfosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDeLannee }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDeLannee }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getLivreDeLannee")
    public JAXBElement<GetLivreDeLannee> createGetLivreDeLannee(GetLivreDeLannee value) {
        return new JAXBElement<GetLivreDeLannee>(_GetLivreDeLannee_QNAME, GetLivreDeLannee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDeLanneeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDeLanneeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getLivreDeLanneeResponse")
    public JAXBElement<GetLivreDeLanneeResponse> createGetLivreDeLanneeResponse(GetLivreDeLanneeResponse value) {
        return new JAXBElement<GetLivreDeLanneeResponse>(_GetLivreDeLanneeResponse_QNAME, GetLivreDeLanneeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDuMois }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDuMois }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getLivreDuMois")
    public JAXBElement<GetLivreDuMois> createGetLivreDuMois(GetLivreDuMois value) {
        return new JAXBElement<GetLivreDuMois>(_GetLivreDuMois_QNAME, GetLivreDuMois.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDuMoisResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDuMoisResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getLivreDuMoisResponse")
    public JAXBElement<GetLivreDuMoisResponse> createGetLivreDuMoisResponse(GetLivreDuMoisResponse value) {
        return new JAXBElement<GetLivreDuMoisResponse>(_GetLivreDuMoisResponse_QNAME, GetLivreDuMoisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRetour }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRetour }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getRetour")
    public JAXBElement<GetRetour> createGetRetour(GetRetour value) {
        return new JAXBElement<GetRetour>(_GetRetour_QNAME, GetRetour.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRetourResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRetourResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceanne.ib.fr/", name = "getRetourResponse")
    public JAXBElement<GetRetourResponse> createGetRetourResponse(GetRetourResponse value) {
        return new JAXBElement<GetRetourResponse>(_GetRetourResponse_QNAME, GetRetourResponse.class, null, value);
    }

}
