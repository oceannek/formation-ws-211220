package fr.ib.oceanne.mediatheque1cxf.serveur;



import java.util.Date;

import javax.jws.WebService;

//procedure accessible au client, ce qui sera publié


@WebService
public interface ILivresService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour (int id);
	public Livre getLivreDuMois();
	public Livre[] getLivreDeLannee();
}
