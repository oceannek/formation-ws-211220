package fr.ib.oceanne.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.oceanne.mediatheque1cxf.serveur.ILivresService;


public class ClientMain {

	public static void main(String[] args) {
		System.out.println("Client livres");
		//cxf implemente la norme javax-ws
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");  //Cette adresse pourait se trouver en Asie
		factory.setServiceClass(ILivresService.class);   //lie au procedures à dispositions dans l'interface
		//Intercepteur(2 objets) pour afficher les requêtes entre client et serveur dans la console
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivresService livresService = factory.create(ILivresService.class);
		System.out.println(livresService.getInfos());
		System.out.println("Livre 4 empruntable : " +livresService.estEmpruntable(4));
		try {
			System.out.println("Livre -3 empruntable : " +livresService.estEmpruntable(-3));
		} catch (Exception ex) {
			System.out.println("Exception : " +ex.getMessage());
		}
		System.out.println("Retour du livre 4 :" +livresService.getRetour(4));
		System.out.println("Livre du mois : " +livresService.getLivreDuMois());
		System.out.println("Livre de mars : " +livresService.getLivreDeLannee()[2].getTitre());
		
	}

}
