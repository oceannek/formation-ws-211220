package fr.ib.oceanne.mediatheque1cxf.serveur;

import java.beans.Transient;
import java.io.Serializable;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlType;

//Afin d'organiser lesb retours dans le corps soap
@XmlType(propOrder= {"titre", "auteur", "annee", "image"})
public class Livre implements Serializable{

	private static final long serialVersionUID = 1L;
	private String titre;
	private String auteur;
	private int annee;
	private DataHandler image;
	
	public Livre(String t, String a, int ann) {
		this.titre = t;
		this.auteur = a;
		this.annee = ann;
	}

	public Livre() {
	}

	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	
	public DataHandler getImage() {
		return image;
	}

	public void setImage(DataHandler image) {
		this.image = image;
	}

	@Transient
	public String getConfidentialInternalCode() {
		return "jdskncjksdksl,kc ,";
	}
	
	@Override
	public String toString() {
		return "Livres [titre=" + titre + ", auteur=" + auteur + ", annee=" + annee + "]";
	} 

	
	
}
