package fr.ib.oceanne.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.oceanne.mediatheque3ejb.iDvdtheque;

@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private iDvdtheque dvdtheque;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		resp.setContentType("text/html");
		
		Writer out = resp.getWriter();
		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDthèque</h1>");
		out.write("<p>" +dvdtheque.getInfos()+ "</p>");
		out.write("<p> La Dvdtheque est ouverte à 9 h 50 ?" +dvdtheque.ouvertA(LocalTime.of(9, 50))+ "</p>");
		out.write("<p> Quand ?" +dvdtheque.getDerniereInterrogation()+ "</p>");
		out.write("</body></html>");
		out.close();
	}

	//cherche l'EJB
	@EJB(lookup="ejb:/MediathequeEjb/Dvds!fr.ib.oceanne.mediatheque3ejb.iDvdtheque")
	public void setDvdteque(iDvdtheque dvdtheque) {
		this.dvdtheque = dvdtheque;
	}
	
	

}

	

	
	
	

	
