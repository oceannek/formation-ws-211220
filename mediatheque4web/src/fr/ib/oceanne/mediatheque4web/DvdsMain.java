package fr.ib.oceanne.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.oceanne.mediatheque3ejb.iDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
			//context permet d'accéder à l'anuaire : JNDI
			Context context = new InitialContext();
			iDvdtheque dvdtheque =(iDvdtheque) context.lookup("ejb:/MediathequeEjb/Dvds!fr.ib.oceanne.mediatheque3ejb.iDvdtheque");
			System.out.println("Information : " +dvdtheque.getInfos());
			System.out.println("La bibliotheque est ouverte à 17 h 30 : " +dvdtheque.ouvertA(LocalTime.of(17, 30)));
			
			context.close();

		} catch (Exception ex) {
			System.err.println(ex);
			ex.printStackTrace();
		}

	}

}
