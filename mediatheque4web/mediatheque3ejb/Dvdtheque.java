package fr.ib.oceanne.mediatheque3ejb;


import java.time.LocalTime;

import javax.ejb.Stateless;

//designe un bean de session
@Stateless(name = "Dvds", description="Opération pour la DVDthèque")
public class Dvdtheque implements iDvdtheque{
	public String getInfos() {
		return "Nouvelle DVDtheque, ouverte de 10 h à 18h";
	}
	
	public boolean ouvertA(LocalTime t) {
			return t.isAfter(LocalTime.of(10, 0)) &&
					t.isBefore(LocalTime.of(18, 0));
		}
	}

